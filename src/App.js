import React, { Component } from 'react';
import './App.css';
import { userService } from './userService';
import Slider from './Slider.js'

class App extends Component {  

   constructor(props) {
    super(props);

    this.state = {
        responseData: [] ,
        imageData: {},
        currentImageData : {},
        currentId : "",
        currentCounter : 0,
        currentUrl : "",
    }
    this.interval = ""
  }

  populateImageData = () =>{
    this.state.responseData.map((data) => {
      userService.post(data.id)
        .then((response)=>{
          let data = Object.assign({},this.state.imageData)
          data[response.data.id] = response.data
          this.setState({
            imageData:data 
          }) 
          if(this.state.responseData.length == Object.keys(this.state.imageData).length)
          {
            this.interval = setInterval(()=>{
              this.slideRight()
            }, 1000);

            this.setState({
              currentUrl:this.state.imageData[this.state.currentId].url
            }) 

          }
        }).catch((err)=>{
            console.log(err);
        })
    })
  }

  componentDidMount()
  {
      userService.get()
        .then((response)=>{
          this.setState({
            responseData : response.data,
            currentId : response.data[this.state.currentCounter].id
          })
          this.populateImageData()
        }).catch((err)=>{
            console.log(err);
        })
 }

  componentWillMount()
  {
        let element = document.getElementById("root")
        let height = window.innerHeight - 10
        let width = window.innerWidth - 10
        element.style.height = height +"px"
        element.style.width = width + "px"
  }

  slideLeft=()=>{
    let counter = 0;
    if(this.state.currentCounter == 0)
    {
        counter = this.state.responseData.length - 1;
    }
    else
    {
        counter = this.state.currentCounter - 1;
    }

    let id = this.state.responseData[counter].id;
    let url = this.state.imageData[id].url;

    this.setState({
      currentCounter : counter,
      currentId : id,
      currentUrl : url
    })

    clearInterval(this.interval)
    this.interval = setInterval(()=>{
              this.slideRight()
            }, 1000);

  }

  slideRight=()=>{
    let counter = 0;
    if(this.state.currentCounter == (this.state.responseData.length - 1))
    {
        counter = 0;
    }
    else
    {
        counter = this.state.currentCounter + 1;
    }

    let id = this.state.responseData[counter].id;
    let url = this.state.imageData[id].url;

    this.setState({
      currentCounter : counter,
      currentId : id,
      currentUrl : url
    })

    clearInterval(this.interval)
    this.interval = setInterval(()=>{
              this.slideRight()
            }, 1000);
  }

  render() {
    return (
      <div className="App">
        <div className ="headerDiv">
          <span> Avengers Avenue </span>
        </div>
        <Slider currentImageUrl = {this.state.currentUrl} slideRight={this.slideRight} slideLeft = {this.slideLeft}/>
      </div>
    );
  }
}

export default App;
