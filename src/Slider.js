import React, { Component } from 'react';

class Slider extends Component {    
 constructor(props) {
    super(props);
  }

  slideLeft=()=>{
    this.props.slideLeft()
  }

  slideRight=()=>{
    this.props.slideRight()
  }

  render() {
    return (
      <div className="SliderOuterDiv">
        <div className="leftSlider" onClick = {this.slideLeft}>
          <img src = "https://s3-ap-southeast-1.amazonaws.com/he-public-data/Left%20Control577660a.png" className="leftSliderImage"></img>
        </div>
        <div className="imageDiv">
          <img src = {this.props.currentImageUrl} className="mainImage"></img>
        </div>
        <div className="rightSlider" onClick = {this.slideRight}>
          <img src = "https://s3-ap-southeast-1.amazonaws.com/he-public-data/Right%20Control3fc6d2d.png" className="rightSliderImage"></img>
        </div>
      </div>
    );
  }
}

export default Slider;
