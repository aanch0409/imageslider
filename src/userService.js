import axios from 'axios';
import config from './config/config';

export const userService = {
    get,
    post
};

var bodyParameters = {
   key: "value"
}

const token = "fc1be0ce7f79cfe74502163bbc76613e";

function get(){
    return axios.get(config.baseUrl, { headers: {"Authorization" : `Bearer ${token}`} }).then((response)=>{
       return response;
    }).catch((err)=>{
       console.log(err);
    })
}

function post(id){
    return axios.post(config.baseUrl + id ,bodyParameters, { headers: {"Authorization" : `Bearer ${token}`} }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log(err);
    })
}